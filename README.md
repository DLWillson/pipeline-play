The published product of this project's pipeline:
http://dlwillson.gitlab.io/pipeline-play/

Further reading on CI / CD:
- [GitLab CI Examples](https://docs.gitlab.com/ee/ci/examples/README.html)
- [Rubber Chicken CI](http://www.jamesshore.com/Blog/Continuous-Integration-on-a-Dollar-a-Day.html)
- [Continuous Integration, by Martin Fowler](https://www.martinfowler.com/articles/continuousIntegration.html)
- [Continuous Delivery, by Farley and Humble](https://www.amazon.com/dp/0321601912)

NOT every push?

- Add "only:" or "when:" rule to filter jobs
- Add "[skip ci]" or "[ci skip]" to commit message
